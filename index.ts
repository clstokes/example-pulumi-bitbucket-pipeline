import * as random from "@pulumi/random";

export const randomPassword = new random.RandomPassword("main", {
    length: 12,
}).result;
